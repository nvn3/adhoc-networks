# Adhoc-Networks

Adhoc Networks (ET4388) 2019

The manual for operation can be found at `./Manual/ET4388_Lab_Manual.pdf`.

<!-- ## TL;DR

### Building and running the server

Navigate to the path `<repository-path>/Source/Server`, run the command `$ make`.

Once the server is built successfully, run the following command:

`$ ./tcpserver <NUMBER OF BOTS>`

The following should be seen on terminal:

```
∗∗∗∗∗∗∗∗∗∗∗∗∗∗∗ Servercontrol program ( Adhoc networking course )
∗ Binding done
∗ Waiting for bots to connect
− Accepted connection
− Bot with ID : <2> Connected
∗∗∗∗∗∗∗∗∗∗∗∗
In order to send command to a specific connected robot type the ID of the bot you wish to communicate with and
select the appropriate command from the menu.
Enter Bot ID to send the packet
Enter the command (1-12) to the bot-2:
1 . Move forward
2 . Move forward for time in seconds
3 . Move reverse
4 . Move reverse for time in seconds
5 . Move left time
6 . Move right time
7 . Stop the bot
8. Get obstacle distance front
9. Get RSSI value
10. Get ID
11. Execute commands from a file (cmd file.txt)
``` -->